/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package Server;
import java.awt.*;
import java.net.*;
import java.io.*;
import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Server extends javax.swing.JDialog {
    private JPanel lienzo;
    private String inputLine;
    private DataInputStream entrada;
    private final  int PUERTO = 5432;
    private ServerSocket serverSocket = null;
    Mensajes mensajeHilo = new Mensajes();
    /**
     * Creates new form Server
     */
    public Server(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle(":: SERVER ::");
        setSize(450,750);
        
    }
    
    public void pintar(Graphics g, String input) {
        super.paintComponents(g);
        int x = getWidth() / 2;
        int y = getHeight() / 2;
        int radio = 100;
        switch (input) {
            case "11":
                g.setColor(Color.BLUE);
                g.fillOval(150, 200, radio * 2, radio * 2);
                //g.fillOval(210, 70, 80, 80);
                break;
            case "12":
                g.setColor(Color.GREEN);
                g.fillOval(150, 200, radio * 2, radio * 2);
                //g.fillOval(210, 70, 80, 80);
                break;
            case "13":
                g.setColor(Color.RED);
                g.fillOval(150, 200, radio * 2, radio * 2);
                //g.fillOval(210, 70, 80, 80);
                break;
            case "21":
                g.setColor(Color.BLUE);
                g.fillRect(150, 200, radio * 2, radio);
                //g.fillRect(210, 80, 80, 60);
                break;
            case "22":
                g.setColor(Color.GREEN);
                g.fillRect(150, 200, radio * 2, radio);
                //g.fillRect(210, 80, 80, 60);
                break;
            case "23":
                g.setColor(Color.RED);
                g.fillRect(150, 200, radio * 2, radio);
                //g.fillRect(210, 80, 80, 60);
                break;
            default:
                System.out.println("Figura no encontrada");
                break;
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtPeticiones = new javax.swing.JTextArea();
        iniciarServer = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        txtPeticiones.setColumns(20);
        txtPeticiones.setRows(5);
        jScrollPane1.setViewportView(txtPeticiones);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 440, 400, 167);

        iniciarServer.setText("INICIAR SERVIDOR");
        iniciarServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                iniciarServerActionPerformed(evt);
            }
        });
        getContentPane().add(iniciarServer);
        iniciarServer.setBounds(120, 630, 150, 23);

        jLabel1.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel1.setText("FIGURA SOLICITADA");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(110, 20, 180, 22);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void iniciarServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_iniciarServerActionPerformed
        // TODO add your handling code here:
        mensajeHilo = new Mensajes();
        mensajeHilo.start();
        System.out.println("Servidor iniciado");
    }//GEN-LAST:event_iniciarServerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Server dialog = new Server(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton iniciarServer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtPeticiones;
    // End of variables declaration//GEN-END:variables
    class Mensajes extends Thread{
        public void run(){
            try{
                serverSocket=  new ServerSocket(PUERTO);
            }
            catch(IOException e){
                System.err.println("No se puede escuchar por el puerto: "+ PUERTO);
                System.exit(1);
            }
            Socket clientSocket = null;
            try{
                clientSocket = serverSocket.accept();
                entrada=new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            }
            catch(IOException e){
                System.err.println("Conexion fallida");
                System.exit(1);
            }
            while(true){
                try{
                    inputLine = entrada.readLine();
                }
                catch(IOException e){
                    System.err.println("El mensaje no pudo ser enviado");
                }
                if(inputLine.equals("Exit")){
                    txtPeticiones.setText("Adios ya me voy");
                    break;
                }
                else if(!inputLine.equals(null)){
                        Graphics g = getGraphics();
                        if(g!=null){
                            pintar(g, inputLine);
                        }
                }
                String descripcion="";
                switch (inputLine) {
                    case "11":
                        descripcion="Circulo azul";
                        break;
                    case "12":
                        descripcion="Circulo verde";
                        break;
                    case "13":
                        descripcion="Circulo rojo";
                        break;
                    case "21":
                        descripcion="Rectangulo azul";
                        break;
                    case "22":
                        descripcion="Rectangulo verde";
                        break;
                    case "23":
                        descripcion="Rectangulo rojo";
                        break;
                }
                txtPeticiones.append("Codigo: "+inputLine+ ", ("+descripcion+") \n");
            }
            try{
                clientSocket.close();
                entrada.close();
            }
            catch(IOException e){
                System.err.println("No se pudo cerrar el socket o stream");
            }
        }
    }
}
